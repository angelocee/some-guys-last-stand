Angelo Cabading 11/10/2019 Professor Price
Assignment #4: Individual 2D Game : Some Guy’s Last Stand
Some Guy’s Last Stand is a simple pixelated top-down zombie wave survival game inspired by Stardew Valley’s arcade bar game, “Journey of the Prairie King”. You use the WASD keys to move, and the arrow keys to shoot bullets at their respective directions. Shooting can also be done diagonally by pressing two arrow keys perpendicular to each other at the same time. For Instance: pressing the up and left arrow keys at the same time shoots at the 135 degree direction. The player starts off with 3 lives and a pistol that holds infinite ammo. Zombies have a small probability of dropping submachine guns, shotguns, and health drops. These weapon drops have very reasonable and common functionalities. Submachine guns increases the players rate of fire, while shotguns spread 3 bullets at a time at a shorter range. The game is set to 5 waves for the sake of efficiency and completing the game. However, difficulty rises as the number of waves increases, and dying completes the game as well. Enjoy!
Bibliography
All graphics have been made by myself through photoshop. These include:
Some Guy’s Sprites
Zombie Sprites
Bullets
Clutter Objects such as rocks and grass
Weapon and health drop sprites
The zombie and Some Guy’s sprites are modified versions of YouTuber CouchFerret’s player sprites from his “Unity Top Down Shooting Game Tutorials”, where I learned how to animate these sprites for directional movement.
Borrowed Assets:
Pixel Fonts by Tiny Worlds
Retro Sound Effects by Zero Rare (Levi Moore)
Gunshot Sounds by Into The Void Sound Design
Codes were taken from various online sources, such as StackExchange and YouTube tutorials, and were then modified by myself to fulfill my own game’s needs. These sources include:
Multiple stack exchange searches
Multiple tutorials by:
Brackeys
CouchFerret Makes Games
Code Monkey
Blackthornprod
and multiple tutorials from the Unity YouTube Channel
