﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    public AudioClip clip;
    private Transform playerPos;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Vector3 cameraZPos = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);

        if (collision.gameObject.CompareTag("Player"))
        {
            AudioSource.PlayClipAtPoint(clip, cameraZPos, 1f);
            Destroy(gameObject);
        }
    }
}
