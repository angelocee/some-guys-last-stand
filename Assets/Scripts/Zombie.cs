﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public float speed;
    System.Random rand = new System.Random();
    private Transform playerPos;
    public Animator animator;
    private Vector3 movement;
    public int HP = 3;
    public GameObject shotgunPrefab, uziPrefab, healthPrefab;
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        playerPos = GameObject.FindWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerPos != null)
        {
            transform.position = Vector2.MoveTowards(transform.position, playerPos.position, speed * Time.deltaTime);
            movement = new Vector3(playerPos.localPosition.x - transform.position.x, playerPos.localPosition.y - transform.position.y, transform.localScale.z);

        }

        if (movement.magnitude > 1.0f)
        {
            movement.Normalize();
        }

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Magnitude", movement.magnitude);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 cameraZPos = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);

        if (collision.gameObject.tag == "Bullet")
        {
            HP -= 1;
            AudioSource.PlayClipAtPoint(clip, cameraZPos, 1f);
            Vector2 difference = transform.position - collision.transform.position;
            transform.position = new Vector2(transform.position.x + difference.x, transform.position.y + difference.y);
        }
        if (HP <= 0)
        {
            Destroy(gameObject);
            int dropRate = rand.Next(0, 100);
            if (dropRate < 5)
            {
                GameObject drop = Instantiate(shotgunPrefab, transform.position, Quaternion.identity);
            }
            if (dropRate > 95)
            {
                GameObject drop = Instantiate(uziPrefab, transform.position, Quaternion.identity);
            }
            if ((dropRate >= 5) && (dropRate <= 10))
            {
                GameObject drop = Instantiate(healthPrefab, transform.position, Quaternion.identity);
            }
        }
    }

}
