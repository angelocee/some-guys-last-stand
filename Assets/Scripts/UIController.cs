﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
	// Start Button
	public void StartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	// Quit Button
	public void QuitGame()
	{
		Application.Quit();
		Debug.Log("You have quit the game.");
	}
}
