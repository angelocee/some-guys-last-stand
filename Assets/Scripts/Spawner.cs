﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING };
    public GameObject Instructions;
    public GameObject WaveUI;
    public int waveNum;
    public int zombie2Chance = 0;
    public int zombie3Chance = 100;
    System.Random rnd = new System.Random();


    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform enemy, enemy2, enemy3;
        public int count;
        public float rate;
    }

    public Wave[] waves;
    public Transform[] spawnPoint;
    private int nextWave = 0;

    public float timeBetweenWaves = 5.0f;
    public float waveCountDown;

    private float searchCountdown = 1.0f;

    private SpawnState state = SpawnState.COUNTING;

    void Start()
    {
        waveCountDown = timeBetweenWaves;
        waveNum = 1;
    }

    private void Update()
    {
        if (state == SpawnState.WAITING)
        {
            if (!EnemyIsAlive())
            {
                WaveCompleted();
            }
            else
            {
                return;
            }
        }

        if (waveCountDown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        Debug.Log("Wave Complete!");

        state = SpawnState.COUNTING;
        waveCountDown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            Invoke("Complete", 3);
        }
        else
        {
            waveNum++;
            zombie2Chance += 3;
            zombie3Chance -= 3;
            nextWave++;
        }
    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0.0f)
        {
            searchCountdown = 1.0f;
            if(GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        Debug.Log("Spawning Wave: " + _wave.name);
        WaveUI.GetComponent<Text>().text = waveNum.ToString();
        state = SpawnState.SPAWNING;
        Instructions.SetActive(false);

        for (int i = 0; i < _wave.count; i++)
        {
            int enemyType = rnd.Next(0, 100);
            if (enemyType < zombie2Chance)
            {
                SpawnEnemy(_wave.enemy2);
            }
            if (enemyType > zombie3Chance)
            {
                SpawnEnemy(_wave.enemy3);
            }

            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1.0f / _wave.rate);
        }
        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    {
        int rand = Random.Range(0, spawnPoint.Length);
        Instantiate(_enemy, spawnPoint[rand].position, Quaternion.identity);
    }

    void Complete()
    {
        Debug.Log("All Waves Complete!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}


