﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private AudioSource bang;
    public Animator topAnimator;
    public Animator bottomAnimator;
    public GameObject bulletPrefab;
    public Vector3 movement;
    public GameObject bullet, bullet1, bullet2;
    public Transform FirePointUp, FirePointUp1, FirePointUp2;
    public Transform FirePointUpRight, FirePointUpRight1, FirePointUpRight2;
    public Transform FirePointRight, FirePointRight1, FirePointRight2;
    public Transform FirePointDownRight, FirePointDownRight1, FirePointDownRight2;
    public Transform FirePointDown, FirePointDown1, FirePointDown2;
    public Transform FirePointDownLeft, FirePointDownLeft1, FirePointDownLeft2;
    public Transform FirePointLeft, FirePointLeft1, FirePointLeft2;
    public Transform FirePointUpLeft, FirePointUpLeft1, FirePointUpLeft2;
    private double nextFire;
    public bool Uzi, Shotgun = false;
    public int UziAmmo, ShotgunAmmo;
    public GameObject uziUI, shotgunUI;
    public GameObject pistolIcon, uziIcon, shotgunIcon, pistolUnequipped, uziUnequipped, shotgunUnequipped;

    private void Start()
    {
        bang = GetComponent<AudioSource>();
    }

    void Update()
    {
        Move();
        Animate();
        Shoot();
        Switch();
        UI();
        uziUI.GetComponent<Text>().text = UziAmmo.ToString();
        shotgunUI.GetComponent<Text>().text = ShotgunAmmo.ToString();
    }

    public void Move()
    {
        movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);
        transform.position = transform.position + movement * Time.deltaTime;

        if (movement.magnitude > 1.0f)
        {
            movement.Normalize();
        }
    }

    public void Animate()
    {
        bottomAnimator.SetFloat("Horizontal", movement.x);
        bottomAnimator.SetFloat("Vertical", movement.y);
        bottomAnimator.SetFloat("Magnitude", movement.magnitude);

        topAnimator.SetFloat("Horizontal", movement.x);
        topAnimator.SetFloat("Vertical", movement.y);
        topAnimator.SetFloat("Magnitude", movement.magnitude);
    }

    public void Switch()
    {
        if (Input.GetButton("Weapon1"))
        {
            if (Uzi == true && Shotgun == false)
            {
                Uzi = false;
            }
            else
            {
                Shotgun = false;
            }
        }
        if (Input.GetButton("Weapon2"))
        {
            if (UziAmmo > 0)
            {
                Shotgun = false;
                Uzi = true;
            }
        }
        if (Input.GetButton("Weapon3"))
        {
            if (ShotgunAmmo > 0)
            {
                Uzi = false;
                Shotgun = true;
            }
        }
    }

    public void UI()
    {
        if (Uzi == true || Shotgun == true)
        {
            pistolIcon.SetActive(false);
        }
        if (Uzi == false && Shotgun == false)
        {
            pistolIcon.SetActive(true);
        }
        if (Uzi == true)
        {
            uziIcon.SetActive(true);
        }
        if (Uzi == false)
        {
            uziIcon.SetActive(false);
        }
        if (Shotgun == true)
        {
            shotgunIcon.SetActive(true);
        }
        if (Shotgun == false)
        {
            shotgunIcon.SetActive(false);
        }
    }

    //pick up weapon
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Shotgun"))
        {
            Debug.Log("Picked up shotgun");
            shotgunUnequipped.SetActive(true);
            shotgunUI.SetActive(true);
            ShotgunAmmo += 20;
        }
        if (collision.gameObject.CompareTag("Uzi"))
        {
            Debug.Log("Picked up uzi");
            uziUnequipped.SetActive(true);
            uziUI.SetActive(true);
            UziAmmo += 100;
        }
    }

    public void Shoot()
    {
        if (UziAmmo <= 0)
        {
            Uzi = false;
        }
        if (ShotgunAmmo <= 0)
        {
            Shotgun = false;
        }

        var fireRate = 0.4;
        // fire up
        if (Input.GetButton("FireUp") && !Input.GetButton("FireRight") && !Input.GetButton("FireLeft") && Time.time > nextFire)
        {
            topAnimator.SetBool("ShootingUp", true);
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointUp.position, Quaternion.Euler(0, 0, 90));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 5f);
                bullet1 = Instantiate(bulletPrefab, FirePointUp1.position, Quaternion.Euler(0, 0, 90));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(-.25f, 4.75f);
                bullet2 = Instantiate(bulletPrefab, FirePointUp2.position, Quaternion.Euler(0, 0, 90));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(.25f, 4.75f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointUp.position, Quaternion.Euler(0, 0, 90));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 3.0f);
            }
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (Input.GetButtonUp("FireUp") || (Input.GetButton("FireUp") && (Input.GetButton("FireRight") || Input.GetButton("FireLeft"))))
        {
            topAnimator.SetBool("ShootingUp", false);
        }
        // fire right
        if (Input.GetButton("FireRight") && !Input.GetButton("FireUp") && !Input.GetButton("FireDown") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointRight.position, Quaternion.identity);
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(5.0f, 0.0f);
                bullet1 = Instantiate(bulletPrefab, FirePointRight1.position, Quaternion.identity);
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(4.75f, 0.25f);
                bullet2 = Instantiate(bulletPrefab, FirePointRight2.position, Quaternion.identity);
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(4.75f, -0.25f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointRight.position, Quaternion.identity);
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(3.0f, 0.0f);
            }        
            topAnimator.SetBool("ShootingRight", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (Input.GetButtonUp("FireRight") || (Input.GetButton("FireRight") && (Input.GetButton("FireUp") || Input.GetButton("FireDown"))))
        {
            topAnimator.SetBool("ShootingRight", false);
        }
        // fire down
        if (Input.GetButton("FireDown") && !Input.GetButton("FireLeft") && !Input.GetButton("FireRight") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointDown.position, Quaternion.Euler(0, 0, -90));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, -5f);
                bullet1 = Instantiate(bulletPrefab, FirePointDown1.position, Quaternion.Euler(0, 0, -90));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(.25f, -4.75f);
                bullet2 = Instantiate(bulletPrefab, FirePointDown2.position, Quaternion.Euler(0, 0, -90));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(-.25f, -4.75f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointDown.position, Quaternion.Euler(0, 0, -90));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, -3.0f);
            }
            topAnimator.SetBool("ShootingDown", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (Input.GetButtonUp("FireDown") || (Input.GetButton("FireDown") && (Input.GetButton("FireRight") || Input.GetButton("FireLeft"))))
        {
            topAnimator.SetBool("ShootingDown", false);
        }
        //fire left
        if (Input.GetButton("FireLeft") && !Input.GetButton("FireUp") && !Input.GetButton("FireDown") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointLeft.position, Quaternion.Euler(0, 180, 0));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-5.0f, 0f);
                bullet1 = Instantiate(bulletPrefab, FirePointLeft1.position, Quaternion.Euler(0, 180, 0));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(-4.75f, -0.25f);
                bullet2 = Instantiate(bulletPrefab, FirePointLeft2.position, Quaternion.Euler(0, 180, 0));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(-4.75f, 0.25f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointLeft.position, Quaternion.Euler(0, 180, 0));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.0f, 0.0f);
            }    
            topAnimator.SetBool("ShootingLeft", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (Input.GetButtonUp("FireLeft") || (Input.GetButton("FireLeft") && (Input.GetButton("FireUp") || Input.GetButton("FireDown"))))
        {
            topAnimator.SetBool("ShootingLeft", false);
        }
        // fire upRight
        if (Input.GetButton("FireUp") && Input.GetButton("FireRight") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointUpRight.position, Quaternion.Euler(0, 45, 45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(3.54f, 3.54f);
                bullet1 = Instantiate(bulletPrefab, FirePointUpRight1.position, Quaternion.Euler(0, 45, 45));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(3.29f, 3.79f);
                bullet2 = Instantiate(bulletPrefab, FirePointUpRight2.position, Quaternion.Euler(0, 45, 45));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(3.79f, 3.29f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointUpRight.position, Quaternion.Euler(0, 45, 45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(3.0f, 3.0f);
            }

            topAnimator.SetBool("ShootingUpRight", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (!Input.GetButton("FireUp") || !Input.GetButton("FireRight"))
        {
            topAnimator.SetBool("ShootingUpRight", false);
        }
        // fire upLeft
        if (Input.GetButton("FireUp") && Input.GetButton("FireLeft") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointUpLeft.position, Quaternion.Euler(0, 135, 45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.54f, 3.54f);
                bullet1 = Instantiate(bulletPrefab, FirePointUpLeft1.position, Quaternion.Euler(0, 135, 45));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.79f, 3.29f);
                bullet2 = Instantiate(bulletPrefab, FirePointUpLeft2.position, Quaternion.Euler(0, 135, 45));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.29f, 3.79f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointUpLeft.position, Quaternion.Euler(0, 135, 45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.0f, 3.0f);
            }
            topAnimator.SetBool("ShootingUpLeft", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (!Input.GetButton("FireUp") || !Input.GetButton("FireLeft"))
        {
            topAnimator.SetBool("ShootingUpLeft", false);
        }
        // fire downRight
        if (Input.GetButton("FireDown") && Input.GetButton("FireRight") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointDownRight.position, Quaternion.Euler(0, 45, -45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(3.54f, -3.54f);
                bullet1 = Instantiate(bulletPrefab, FirePointDownRight1.position, Quaternion.Euler(0, 45, -45));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(3.79f, -3.29f);
                bullet2 = Instantiate(bulletPrefab, FirePointDownRight2.position, Quaternion.Euler(0, 45, -45));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(3.29f, -3.79f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointDownRight.position, Quaternion.Euler(0, 45, -45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(3.0f, -3.0f);
            }
            topAnimator.SetBool("ShootingDownRight", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (!Input.GetButton("FireDown") || !Input.GetButton("FireRight"))
        {
            topAnimator.SetBool("ShootingDownRight", false);
        }
        // fire downLeft
        if (Input.GetButton("FireDown") && Input.GetButton("FireLeft") && Time.time > nextFire)
        {
            bang.Play();
            if (Shotgun == true)
            {
                bullet = Instantiate(bulletPrefab, FirePointDownLeft.position, Quaternion.Euler(0, -135, -45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.54f, -3.54f);
                bullet1 = Instantiate(bulletPrefab, FirePointDownLeft1.position, Quaternion.Euler(0, -135, -45));
                bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.29f, -3.79f);
                bullet2 = Instantiate(bulletPrefab, FirePointDownLeft2.position, Quaternion.Euler(0, -135, -45));
                bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.79f, -3.29f);
                ShotgunAmmo -= 1;
            }
            else
            {
                bullet = Instantiate(bulletPrefab, FirePointDownLeft.position, Quaternion.Euler(0, -135, -45));
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.0f, -3.0f);
            }
            topAnimator.SetBool("ShootingDownLeft", true);
            if (Uzi == true)
            {
                nextFire = Time.time + 0.1;
                UziAmmo -= 1;
            }
            else
            {
                nextFire = Time.time + fireRate;
            }
        }
        if (!Input.GetButton("FireDown") || !Input.GetButton("FireLeft"))
        {
            topAnimator.SetBool("ShootingDownLeft", false);
        }
        // range
        if (Shotgun == true)
        {
            Destroy(bullet, .25f);
            Destroy(bullet1, .25f);
            Destroy(bullet2, .25f);
        }
        else
        {
            Destroy(bullet, 2.0f);
        }
    }
}
