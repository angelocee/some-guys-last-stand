﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SomeGuy : MonoBehaviour
{
    public int HP = 3;
    public GameObject UIHP;
    public AudioClip clip;
    bool dead;

    private void Update()
    {
        UIHP.GetComponent<Text>().text = HP.ToString();
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 cameraZPos = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);

        if (collision.gameObject.tag == "Enemy")
        {
            AudioSource.PlayClipAtPoint(clip, cameraZPos, 1f);

            HP -= 1;
            Debug.Log("Hit");
            Vector2 difference = transform.position - collision.transform.position;
            transform.position = new Vector2(transform.position.x + difference.x, transform.position.y + difference.y);

            if (HP <= 0)
            {
                gameObject.SetActive(false);
                Invoke("Death", 3);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Health"))
        {
            Debug.Log("Picked up some health!");
            HP++;
        }
    }

    public void Death()
    {
        Debug.Log("Died");
        Destroy(gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
